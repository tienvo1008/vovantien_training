<?php

namespace Backend\Repositories\Images;

use Backend\Models\Image;
use Backend\Repositories\EloquentRepository;

class ImageRepository extends EloquentRepository implements ImageInterface
{
    /**
     * get model
     * @return string
     */
    function getModel()
    {
        return Image::class;
    }

    public function find($id)
    {
        return $this->_model->find($id);
    }
}
