<?php

namespace Backend\Repositories\ReleaseNumber;

interface ReleaseNumberInterface
{
    /**
     * Get 5 posts hot in a month the last
     * @return mixed
     */
    public function getAll();

    public function delete($id);

    public function create(array $attributes);

    public function update($id, array $attributes);
}
