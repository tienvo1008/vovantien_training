<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            array( 'name' => 'abc', 'email' => 'abc@gmail.com', 'password' => bcrypt('123456'),
                'role' => 1, 'status' => 1 ),
            array( 'name' =>   'abcd', 'email' => 'abc1@gmail.com', 'password' => bcrypt('123456'),
                'role' => 2, 'status' => 1 ),
            array( 'name' =>   'abcde', 'email' => 'abc2@gmail.com', 'password' => bcrypt('123456'),
                'role' => 3, 'status' => 1 ),
            array( 'name' =>   'abcdef', 'email' => 'abc3@gmail.com', 'password' => bcrypt('123456'),
                'role' => 2, 'status' => 1 ),
        ]);
    }
}
