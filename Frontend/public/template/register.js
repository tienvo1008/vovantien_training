$(function() {
   $('.date').datepicker({
       format: 'mm-dd-yyyy',
       maxDate: new Date(),
   });
   $('#register').on('click', function (e) {
       e.preventDefault();
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('input[name="_token"]').val()
           }
       });
       $.ajax({
            'url': 'register',
            'data': {
               'name': $('#name').val(),
               'email': $('#email').val(),
               'password': $('#pass').val(),
               'birday': $('#dateOfBird').val(),
               'gender': $('input[name=gender]:checked').val(),
           },
           'type': 'post',
           success: function (data) {
               if(data.errors === false) {
                   window.location.reload()
               }
           },
           error: function (err) {
                console.log(err.responseJSON.errors)
               $('.error').hide();
               if (err.responseJSON.errors.name) {
                   $('.errorName').show().text(err.responseJSON.errors.name.toString());
               }
               if (err.responseJSON.errors.email) {
                   $('.errorEmail').show().text(err.responseJSON.errors.email.toString());
               }
               if (err.responseJSON.errors.password) {
                   $('.errorPassword').show().text(err.responseJSON.errors.password.toString());
               }
           }
       })
   })
});