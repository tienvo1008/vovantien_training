<?php

namespace Backend\Http\Controllers\Post;

use Backend\Http\Controllers\Controller;
use Backend\Http\Requests\AddPostRequest;
use Backend\Http\Requests\UpdatePostRequest;
use Backend\Repositories\Images\ImageRepository;
use Backend\Repositories\Post\PostRepository;
use Backend\Transformer\PostTransformer as ApiPost;
use Exception;
use Illuminate\Support\Facades\Storage;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class PostController extends Controller
{
    protected $postRepository;

    protected $fractal;

    protected $postTransformer;

    protected $imageRepository;

    public function __construct(PostRepository $postRepository,
                                Manager $fractal,
                                ApiPost $postTransformer,
                                ImageRepository $imageRepository)
    {
        $this->postRepository = $postRepository;
        $this->fractal = $fractal;
        $this->postTransformer = $postTransformer;
        $this->imageRepository = $imageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $postPaginate = $this->postRepository->getAll();
            $posts = $this->postRepository->getAll();
            $posts = new Collection($posts, $this->postTransformer);
            $posts = $this->fractal->createData($posts);

            $response = [
                'pagination' => [
                    'total'        => $postPaginate->total(),
                    'per_page'     => $postPaginate->perPage(),
                    'current_page' => $postPaginate->currentPage(),
                    'last_page'    => $postPaginate->lastPage(),
                    'from'         => $postPaginate->firstItem(),
                    'to'           => $postPaginate->lastItem()
                ],
                'data' => $posts->toArray(),
            ];

            return response($response);

        } catch (\Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。',
                ]
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPostRequest $request)
    {
        try {

            $arrPost = array(
                'categories_id' => $request->get('categories_id'),
                'release_number_id' => $request->get('release_number_id'),
                'image_id' => $request->get('image_id'),
                'user_id' => $request->get('user_id'),
                'title' => $request->get('title'),
                'public_date' => $request->get('public_date'),
                'content' => $request->get('content'),
                'status' => $request->get('status'),
                'description' => $request->get('description'),
            );

            $post = $this->postRepository->create($arrPost);

            if ($post) {
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => false,
                    ]
                ]);
            } else {
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => '情報処理の中に、エーラが発生しました',
                    ]
                ]);
            }
        } catch (Exception $exception) {
            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $postUpdate = $this->postRepository->showPost($id);

            return response()->json([
                'data' => $postUpdate->toArray(),
                'error' => [
                    'status' => false
                ]
            ]);
        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。',
                ]
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        try {
            $post = $this->postRepository->find($id);
            $old_image_id = (int) $post->image_id;
            $image = $this->imageRepository->find($old_image_id);
            $url = !empty($image) ?  str_replace('/storage/', '/public/', $image->url) : '';
            $arrPost = array(
                'categories_id' => $request->get('categories_id'),
                'release_number_id' => $request->get('release_number_id'),
                'image_id' => $request->get('image_id'),
                'user_id' => $request->get('user_id'),
                'title' => $request->get('title'),
                'public_date' => $request->get('public_date'),
                'content' => $request->get('content'),
                'status' => $request->get('status'),
                'description' => $request->get('description'),
            );
            $post = $this->postRepository->update($id, $arrPost);
            if ($post) {
                if(!empty($image) && $old_image_id !== $arrPost['image_id']) {
                    $this->imageRepository->delete($image->id);
                    Storage::delete($url);
                }
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => false
                    ]
                ]);
            } else {
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => '情報処理の中に、エーラが発生しました。',
                    ]
                ]);
            }
        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました' . $exception->getMessage(),
                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $post = $this->postRepository->find($id);
            $image_id = $post->image_id;
            $image = $this->imageRepository->find($image_id);
            $url = str_replace('/storage/', '/public/', $image->url);
            $this->imageRepository->delete($image->id);
            Storage::delete($url);
            if($this->postRepository->delete($id)) {

                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => false,
                    ]
                ]);
            }
        } catch (\Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'message' => '情報処理の中に、エーラが発生しました。',
                ]
            ]);
        }
    }
}
