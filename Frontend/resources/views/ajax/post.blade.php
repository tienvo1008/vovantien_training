<div class="block">
    @foreach($posts as $post)
        <div class="item">
            <div class="images" >
                @if($post->image == null)
                    <img src="storage/media/image-not-found.jpg" alt="">
                @else
                    <img src="{!! 'http://adminsite.local' . $post->image->url !!}" alt="">
                @endif
            </div>
            <div class="content">
                <h2>
                    <a href="{{ route('post_detail', $post->id ) }}" class="title">{!! str_limit($post->title, 35) !!}</a>
                </h2>
                <p class="norm">{!! str_limit(strip_tags($post->content), $limit = 60, $end = '.') !!}</p>
            </div>
        </div>
    @endforeach
</div>
