<?php

namespace Backend\Http\Controllers\Image;

use Backend\Http\Controllers\Controller;
use Backend\Repositories\Images\ImageRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    protected $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function store(Request $request)
    {
        try {
            if ($request->file('file')) {
                $file = $request->file('file');
                $originalName = $file->getClientOriginalName();
                $newName = 'media' . time() . $originalName;
                $url = '/storage/media/' . $newName;
                $image = array(
                    'name' => $newName,
                    'url' => $url,
                );

                $imageModel = $this->imageRepository->create($image);

                if ($imageModel) {
                    $file->move(storage_path() . '/app/public/media/', $newName);

                    return response()->json([
                        'data' => $imageModel->id,
                        'error' => [
                            'status' => false,
                        ]
                    ], 200);
                }
            } else {
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => 'この画像はアップロードができません。',
                    ]
                ]);
            }
        } catch (Exception $e) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return $this->imageRepository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $image = $this->imageRepository->find($id);
            $url = str_replace('/storage/', '/public/', $image->url);
            if ($this->imageRepository->delete($id)) {
                Storage::delete($url);
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => false,
                    ]
                ]);
            } else {
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => '',
                    ]
                ]);
            }
        } catch (Exception $exception) {
            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。',
                ]
            ]);
        }
    }
}
