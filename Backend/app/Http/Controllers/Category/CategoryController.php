<?php

namespace Backend\Http\Controllers\Category;

use Backend\Http\Controllers\Controller;
use Backend\Repositories\Category\CategoryRepository;
use Backend\Transformer\CategoryChildTransformer;
use Exception;
use Illuminate\Http\Request;
use League\Fractal\Manager;

class CategoryController extends Controller
{
    protected $fractal;

    protected $categoryChildTransformer;

    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository,
                                CategoryChildTransformer $categoryChildTransformer,
                                Manager $fractal)
    {
        $this->categoryRepository = $categoryRepository;
        $this->categoryChildTransformer = $categoryChildTransformer;
        $this->fractal = $fractal;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $category = $this->categoryRepository->getAll();
            return response()->json([
                'data' => $category,
                'error' => [
                    'status' => false,
                ]
            ]);
        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showParent()
    {
        try {
            $cateParent = $this->categoryRepository->showParent();

            return response()->json([
                'data' => $cateParent,
                'error' => [
                    'status' => false,
                ]
            ]);
        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }
    }

    public function showChild($id)
    {
        try {
            $cateChild = $this->categoryRepository->showChild($id);

            return response()->json([
                'data' => $cateChild,
                'error' => [
                    'status' => false,
                ]
            ]);
        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }
    }
}
