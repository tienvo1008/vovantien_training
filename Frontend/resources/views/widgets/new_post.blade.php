<div class="block">
    <div class="block-left">
        <div class="block-item">
            <div class="b-images">
                @if($newPost{0}->image == null)
                    <img src="storage/media/image-not-found.jpg" alt="">
                @else
                    <img src="{!! 'http://adminsite.local' . $newPost{0}->image->url !!}" alt="">
                @endif
                <div class="content">
                    <h2 class="title-left">
                        <a class="title" href="{{ route('post_detail', $newPost{0}->id ) }}">
                            {!! $newPost{0}->title !!}
                        </a>
                    </h2>
                    <p>
                        {!! date('Y年m月d日', strtotime($newPost{0}->public_date)) !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="block-right">
        <div class="item">
            <div class="images">
                @if($newPost{1}->image == null)
                    <img src="storage/media/image-not-found.jpg" alt="">
                @else
                    <img src="{!! 'http://adminsite.local' . $newPost{1}->image->url !!}" alt="">
                @endif
                <div class="content">
                    <h2 class="content-title">
                        <a class="title" href="{{ route('post_detail', $newPost{1}->id ) }}">
                            {!! $newPost{1}->title !!}
                        </a>
                    </h2>
                    <p class="content-norm">
                        {!! date('Y年m月d日', strtotime($newPost{1}->public_date)) !!}
                    </p>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="images">
                @if($newPost{2}->image == null)
                    <img src="storage/media/image-not-found.jpg" alt="">
                @else
                    <img src="{!!   'http://adminsite.local' . $newPost{2}->image->url !!}" alt="">
                @endif
                <div class="content">
                    <h2 class="content-title">
                        <a class="title" href="{{ route('post_detail', $newPost{2}->id ) }}">
                            {!! $newPost{2}->title !!}
                        </a>
                    </h2>
                    <p class="content-norm">
                        {!! date('Y年m月d日', strtotime($newPost{2}->public_date)) !!}
                    </p>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="images">
                @if($newPost{3}->image == null)
                    <img src="storage/media/image-not-found.jpg" alt="">
                @else
                    <img src="{!! 'http://adminsite.local' . $newPost{3}->image->url !!}" alt="">
                @endif
                <div class="content">
                    <h2 class="content-title">
                        <a class="title" href="{{ route('post_detail', $newPost{3}->id ) }}">
                            {!! $newPost{3}->title !!}
                        </a>
                    </h2>
                    <p class="content-norm">
                        {!! date('Y年m月d日', strtotime($newPost{3}->public_date)) !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
