<?php
namespace Backend\Transformer;

use Backend\Models\Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    public function transform(Post $post)
    {

        $arrStatus = array(
            1 => '公開',
            2 => '非公開',
            3 => '下書き'
        );

        return [
            'id' =>  $post->id,
            'title' => $post->title,
            'public_date' => $post->public_date,
            'content' => $post->content,
            'status' => $arrStatus[$post->status] ?? null,
            'description' => $post->description,
            'category_child' => $post->categories_id,
            'category_child_name' => $post->category->name ?? null,
            'category_parent' => $post->category->parent->id ?? null,
            'category_parent_name' => $post->category->parent->name ?? null,
            'user' => $post->user->name ?? null,
            'release_name' => $post->releaseNumber->name ?? null,
            'thumbnail' => $post->image->url ?? null,
            'date_create' => date('Y-m-d', strtotime($post->created_at)),
            'date_update' => date('Y-m-d', strtotime($post->updated_at)),
        ];
    }
}
