<?php

namespace Backend\Repositories\Post;

use Backend\Models\Post;
use Backend\Models\Setting;
use Backend\Repositories\EloquentRepository;
use Exception;

class PostRepository extends EloquentRepository implements PostInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Post::class;
    }

    public function find($id)
    {
        return $this->_model->findOrFail($id);
    }

    public function showPost($id)
    {
        return $this->_model->with('category.parent', 'releaseNumber', 'image')->findOrFail($id);
    }

    public function delete($id)
    {
        return $this->_model->findOrFail($id)->delete();
    }

    public function getAll()
    {
        $settingPage = Setting::first();
        $per_page = empty($settingPage->post_page) ? 20 : $settingPage->post_page;
        return $this->_model->with('category', 'releaseNumber', 'image', 'user')->
        latest()->paginate($per_page);
    }
}
