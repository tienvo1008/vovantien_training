<?php

use Illuminate\Database\Seeder;

class UserMembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_members')->insert([
            array( 'name' => 'abc', 'email' => 'abc@gmail.com', 'password' => bcrypt('123'),
                'phone_number' => '009090889', 'birday' => '12/12/2012', 'gender' => '1', 'address' => 'da nang',
                'status' => 1),
            array( 'name' => 'abc', 'email' => 'abc1@gmail.com', 'password' => bcrypt('123'),
                'phone_number' => '009090889', 'birday' => '12/12/2012', 'gender' => '1', 'address' => 'da nang',
                'status' => 1),
            array( 'name' => 'abc', 'email' => 'abc2@gmail.com', 'password' => bcrypt('123'),
                'phone_number' => '009090889', 'birday' => '12/12/2012', 'gender' => '1', 'address' => 'da nang',
                'status' => 1),
            array( 'name' => 'abc', 'email' => 'abc3@gmail.com', 'password' => bcrypt('123'),
                'phone_number' => '009090889', 'birday' => '12/12/2012', 'gender' => '1', 'address' => 'da nang',
                'status' => 1),
        ]);
    }
}
