<?php

namespace Backend\Providers;

use Illuminate\Support\ServiceProvider;
use Backend\Repositories\ReleaseNumber\ReleaseNumberInterface;
use Backend\Repositories\ReleaseNumber\ReleaseNumberRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            ReleaseNumberInterface::class,
            ReleaseNumberRepository::class
        );
    }
}
