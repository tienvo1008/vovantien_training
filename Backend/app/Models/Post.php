<?php

namespace Backend\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'categories_id',
        'release_number_id',
        'image_id',
        'user_id',
        'title',
        'public_date',
        'content',
        'status',
        'description',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }

    public function releaseNumber()
    {
        return $this->belongsTo(ReleaseNumber::class, 'release_number_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
