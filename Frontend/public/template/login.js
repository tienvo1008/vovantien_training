$(function () {
    $('#login').click(function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            }
        });
        $.ajax({
            'url' : 'login',
            'data' : {
                'email' : $('#username').val(),
                'password' : $('#password').val()
            },
            'type' : 'post',
            success: function (data) {
                console.log(data);
                if(data.error === true) {
                    if(data.message.errorLogin  !== undefined) {
                        $('.errorLogin').show().text(data.message.errorLogin.toString());
                    }
                } else {
                    window.location.reload()
                }
            },
            error: function(er) {
                for (var i in er.responseJSON.errors) {
                    $('.errorLogin').append(er.responseJSON.errors[i].toString() + '<br />');
                }
                $('.errorLogin').show();
            }
        })
    })
})