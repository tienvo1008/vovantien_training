<div class="header">
    <div class="header-home">
        @if(Auth::guard('members')->check())
            <form action="{{ route('logout') }}" method="post" id="logout-form">
                @csrf
                <a class="item link" onclick="document.getElementById('logout-form').submit()">
                    {{ Auth::guard('members')->user()->name }}
                </a>
            </form>
        @else
            <a href="#" class="item link" data-toggle="modal" data-target="#myModalRegister">
                メンバー登録
            </a>
            @include('auth.register')
            <span>|</span>
            <a href="#" class="item link" data-toggle="modal" data-target="#myModal">
                ログイン
            </a>
            @include('auth.login')
        @endif
    </div>
    <div class="logo-form">
        <div class="logo">
            <h3>
                <a href="{{ route('home') }}" class="title-logo">総合ジャーナル</a>
            </h3>
        </div>
        <div class="search">
            <form class="form">
                <div class="form-search">
                    <input type="text" placeholder="検索内容の入力。。。"/>
                    <button>検索</button>
                </div>
            </form>
        </div>
        <div class="list">
            <p class="item-norm">文字サイズ</p>
            <div class="item">
                <a href="#" class="item-link">小</a>
            </div>
            <div class="item">
                <a href="#" class="item-link">小</a>
            </div>
            <div class="item">
                <a href="#" class="item-link">小</a>
            </div>
        </div>
    </div>
</div>
<div class="category-menu">
    <div class="category">
        @foreach( $cateMenuDisplay as $item )
        <div class="dropdown">
            <a class="dropbtn">{{ $item->name }}</a>
            <div class="dropdown-content">
                @if($item->children)
                    @foreach($item->children as $value)
                        <a href="#">{{$value->name}}</a>
                    @endforeach
                @endif
            </div>
        </div>
        @endforeach
    </div>
    <div class="category-unable">
        @foreach($cateMenuGlobal as $item)
        <div class="dropdown">
            <a class="dropbtn">{{ $item->name }}</a>
            <div class="dropdown-content">
                @if($item->children)
                    @foreach($item->children as $value)
                        <a href="#">{{ $value->name }}</a>
                    @endforeach
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>
