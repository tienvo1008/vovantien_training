<?php

namespace Backend\Http\Resources;

use Backend\Models\Image as Thumbnail;
use Illuminate\Http\Resources\Json\JsonResource;

class ResourceReleaseNumber extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $url = Thumbnail::find($this->image_id);
        return [
            'id' => $this->id,
            'image_id' => $this->image_id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $url['url'] ? $url['url'] : '',
        ];
    }
}
