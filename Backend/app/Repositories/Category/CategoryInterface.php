<?php

namespace Backend\Repositories\Category;

interface CategoryInterface
{
    public function delete($id);

    public function create(array $attributes);

    public function find($id);
}
