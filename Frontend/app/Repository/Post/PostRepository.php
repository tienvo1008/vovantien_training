<?php
namespace Frontend\Repository\Post;

use Frontend\Constant\Constant;
use Frontend\Models\Post;
use Frontend\Repository\EloquentRepository;
use Frontend\Repository\RepositoryInterface;

class PostRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Post::class;
    }

    public function newPost()
    {
        return $this->_model->with('image')
            ->where('status', '1')
            ->orderBy('id','DESC')
            ->limit(Constant::LIMIT_NEW_POST)
            ->get();
    }

    public function getAll()
    {
        $newPosts = $this->newPost()->toArray();
        $newPosts_id = array();

        foreach ($newPosts as $value) {
            $newPosts_id[] = $value['id'];
        }

        return $this->_model->with('image')
            ->where('status', 1)
            ->whereNotIn('id', $newPosts_id)
            ->paginate(Constant::PAGE_ALL_POST);
    }

    public function showPostDetail($id)
    {
        return $this->_model->findOrFail($id);
    }
}
