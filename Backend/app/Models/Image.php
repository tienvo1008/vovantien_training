<?php

namespace Backend\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'name',
        'url',
    ];

    public $timestamps = false;

    public function releaseNumber()
    {
        return $this->hasMany(ReleaseNumber::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
