<?php

namespace Frontend\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserMember extends Authenticatable
{
    protected $table = 'user_members';

    protected $guard = 'member';

    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'birday', 'gender', 'address', 'status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
