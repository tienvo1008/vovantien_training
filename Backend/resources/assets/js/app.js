import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './pages/App.vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import routes from './router';
import VueAuth from '@websanova/vue-auth';
import auth from './router/auth';
import BootstrapVue from 'bootstrap-vue';
import VueSwal from 'vue-swal'

Vue.use(VueSwal);

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);

axios.defaults.baseURL = 'http://adminsite.local/api';

var router = new VueRouter({
    mode: 'history',
    routes: routes
})

Vue.router = router;
App.router = Vue.router;
Vue.use(VueRouter);

Vue.use(VueAuth, auth);

new Vue({
    el: '#app',
    router: router,
    render: app => app(App)
});
