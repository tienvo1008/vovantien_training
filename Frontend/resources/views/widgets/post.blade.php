<div class="block">
    @foreach($posts as $post)
        <div class="item">
            <div class="images">
                <img src="{!! 'http://adminsite.local' . $post->image->url !!}" alt="">
            </div>
            <div class="content">
                <h2 class="title">
                    {!! str_limit($post->title, 35) !!}
                </h2>
                <p class="norm">
                    {!! $post->content !!}
                </p>
            </div>
        </div>
    @endforeach
</div>
