<?php
namespace Frontend\Repository\Register;

use Frontend\Models\UserMember;
use Frontend\Repository\EloquentRepository;
use Frontend\Repository\RepositoryInterface;

class RegisterRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return UserMember::class;
    }
}
