<?php

namespace Frontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6|max:100',
            'email' => 'required|min:6|max:64',
            'password' => 'required|min:6|max:72',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'ジューザ名を入力してください。',
            'name.min' => 'ジューザ名は 6 文字以下の有効です。',
            'name.max' => 'ジューザ名は１００文字以下の有効です。',
            'email.required' => 'メール メール を入力してください。',
            'email.min' => 'メールは６文字以下の有効です。',
            'email.max' => 'メールは６４文字以下の有効です。',
            'password.required' => 'を入力してください。',
            'password.min' => 'パスワードは6文字以下の有効です。。',
            'password.max' => 'パスワードは７２文字以下の有効です。。',
        ];
    }
}
