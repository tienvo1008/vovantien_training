<?php

namespace Backend\Transformer;

use Backend\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryChildTransformer extends TransformerAbstract
{
    public function transform(Category $category)
    {
        return [
            'child_category' => $category->children,
        ];
    }
}
