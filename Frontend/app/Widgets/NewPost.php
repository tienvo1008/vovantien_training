<?php

namespace Frontend\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Repository\Post\PostRepository;

class NewPost extends AbstractWidget
{
    protected $postRepository;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $newPost = $this->postRepository->newPost();

        return view('widgets.new_post', [
            'config' => $this->config,
            'newPost' => $newPost,
        ]);
    }
}
