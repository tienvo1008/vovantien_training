<?php
namespace Frontend\Repository\ReleaseNumber;

use Frontend\Constant\Constant;
use Frontend\Models\ReleaseNumber;
use Frontend\Repository\EloquentRepository;
use Frontend\Repository\RepositoryInterface;

class ReleaseRepository extends EloquentRepository implements RepositoryInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return ReleaseNumber::class;
    }

    public function getRelease()
    {
        return $this->_model->with('image')
            ->orderBy('id', 'DESC'
            )->limit(Constant::LIMIT_RELEASE)->get();
    }
}
