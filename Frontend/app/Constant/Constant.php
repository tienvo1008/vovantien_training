<?php
namespace Frontend\Constant;

class Constant {
    const MENU_DISPLAY = 1;
    const GLOBAL_DISPLAY = 1;
    const LIMIT_NEW_POST = 4;
    const LIMIT_RELEASE = 5;
    const PAGE_ALL_POST = 2;
}
