@extends('layout.master')
@section('content')
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        @widget('release_number')
    </div>
    <div class="post-new">
        <div class="container-home">
            <div class="heading">
                <h2 class="title">新しい記事</h2>
            </div>
            <div class="content">
                @widget('new_post')
            </div>
        </div>
    </div>
    <div class="article">
        <div class="container-home">
            <div class="heading">
                <h2 class="heading-title">
                    記事
                </h2>
            </div>

            <div class="article-content" id="post-data">
                @include('ajax.post')
            </div>
        </div>
        <div class="paginate">
            <button id="btn_show_more" class="button">もっと見る</button>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        var page = 1;
        $('#btn_show_more').click(function () {
            page++;
            loadMoreData(page);

            function loadMoreData(page) {
                $.ajax(
                    {
                        url: '?page=' + page,
                        type: "get",
                        beforeSend: function () {
                            $('#btn_show_more').show();
                        }
                    })
                    .done(function (data) {
                        if (data.current_page >= data.last_page) {
                            $('#btn_show_more').hide();
                        }
                        $("#post-data").append(data.html);
                    })
                    .fail(function (jqXHR, ajaxOptions, thrownError) {
                        console.log('Error server');
                    });
            }
        });
    </script>
@endsection
