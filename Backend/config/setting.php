<?php

return [
    'limit_login' => 5,
    'bad_request' => 400,
    'unauthorized' => 401,
    'server_error' => 500,
    'success' => 200,
    'status_lock' => 0

];
