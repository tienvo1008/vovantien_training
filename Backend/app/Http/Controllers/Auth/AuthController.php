<?php

namespace Backend\Http\Controllers\Auth;

use Backend\Http\Controllers\Controller;
use Backend\Http\Requests\LoginRequest;
use Backend\Models\User;

use Exception;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $maxLoginAttempts = 5;

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        $user = User::where('email', $request->email)->first();

        if ( empty($user->status) && !empty($user)  ) {
            return response()->json([
                'data' => '',
                'error' => [
                    'status"=' => 'success',
                    'message' => "アカウントをロックしました。管理者に連絡を行ってください。"
                ]
            ], 400);
        }

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            if (!empty($user)) {
                $user->status = 0;
                $user->save();
            }
            return response()->json([
                'data' => '',
                'error' => [
                    "status" => 'success',
                    "message" => "パスワードに5回連続で誤りがあったため、アカウントをロックしました。管理者に連絡を行ってください。"
                ]
            ], 400);
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $this->incrementLoginAttempts($request);
                return response()->json([
                    'status' => 'success',
                    'message' => 'ユーザー情報が正しくない。'
                ], 401);
            }
        } catch (Exception $e) {
            return response()->json([
                'data' => '',
                'error' => [
                    'status' => 'success',
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ], 500);
        }

        return response([
            'status' => 'success'
        ])->header('Authorization', $token);
    }

    public function user()
    {
        return response()->json(auth()->user());
    }

    /**
     * fbjkfbskjfnks
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }

    /**
     *
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return response([
                'data' => '',
                'error' => [
                    'status' => 'success',
                    'message' => 'Logged out Successfully.'
                ]
            ], 200);
        } catch (Exception $ex) {
            return response([
                'data' => '',
                'error' => [
                    'status' => 'success',
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ], 400);
        }
    }
}
