<?php
namespace Frontend\Repository\Category;

use Frontend\Constant\Constant;
use Frontend\Models\Category;
use Frontend\Repository\EloquentRepository;
use Frontend\Repository\RepositoryInterface;

class CategoryRepository extends EloquentRepository implements RepositoryInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Category::class;
    }

    public function showMenuDisplay()
    {
        return $this->_model->select('id', 'name')->where('menu_display', Constant::MENU_DISPLAY)->get();
    }
}
