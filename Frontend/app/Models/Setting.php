<?php

namespace Frontend\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'release_number_page',
        'member_page',
        'post_page',
        'img_page',
        'user_admin_page',
    ];
}
