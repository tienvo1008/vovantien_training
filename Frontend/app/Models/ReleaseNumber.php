<?php

namespace Frontend\Models;

use Illuminate\Database\Eloquent\Model;

class ReleaseNumber extends Model
{
    protected $table = 'release_numbers';

    protected $fillable = [
        'image_id',
        'name',
        'description',
        'thumbnail',
    ];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public $timestamps = false;
}
