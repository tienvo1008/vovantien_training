<div class="modal" id="myModalRegister">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title-login">ログイン</h4>
                <button type="button" data-dismiss="modal">閉じる</button>
            </div>
            <div class="modal-body">
                <form class="login" action="" method="POST" data-type="json">
                    @csrf
                    <div class="form-group">
                        <label>メールアドレス</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="メールの入力">
                    </div>
                    <p style="color:red; display: none" class="error errorName"></p>
                    <div class="form-group">
                        <label>メールアドレス</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="メールの入力">
                    </div>
                    <p style="color:red; display: none" class="error errorEmail"></p>
                    <div class="form-group">
                        <label>パスワード</label>
                        <input type="password" class="form-control" id="pass" name="password" placeholder="パスワードの入力">
                    </div>
                    <p style="color:red; display: none" class="error errorPassword"></p>
                    <div class="form-group">
                        <label>生年月日</label>
                        <input type="text" class="date form-control" id="dateOfBird" name="date" placeholder="生年月日の入力">
                    </div>
                    <div class="form-group">
                        <label>生年月日</label>
                        <div class="radio-group">
                            <label class="male">男<input class="radio-item" type="radio" name="gender" value="1"></label>
                            <label class="female">女<input class="radio-item"  type="radio" name="gender" value="0"></label>
                        </div>
                    </div>
                    <div class="btn-login">
                        <button type="button" class="login" id="register">登録</button>
                        <button type="button" class="close-login" data-dismiss="modal">キャンセル</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
