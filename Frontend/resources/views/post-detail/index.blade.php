@extends('layout.master')
@section('content')
    <div class="show-post">
        <div class="container">
            <div class="title">
                <h4 class="title">{{ $post->title  }}</h4>
            </div>
            <div class="content">
                <p>
                    {!! $post->content !!}
                </p>
            </div>
            <div class="infor">
                <span class="name-user">作成者：{{ $post->user->name }}</span>
                <span class="mark">|</span>
                <span class="release-date">作成日：{{ $post->public_date }}</span>
            </div>

        </div>
    </div>
@endsection