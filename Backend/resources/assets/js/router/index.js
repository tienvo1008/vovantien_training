import Login from '../pages/auth/Login';
import Home from '../components/HomeComponent';

import AdminUser from '../pages/admin/Index';
import Categories from '../pages/categories/Index';
import Images from '../pages/images/Index';

import ReleaseNumber from '../pages/releasenumber/Index';
import AddRelease from  '../pages/releasenumber/Add';
import EditRelease from '../pages/releasenumber/Edit';

import Posts from '../pages/posts/Index';
import AddPost from '../pages/posts/Add';
import EditPost from '../pages/posts/Edit';

import Members from '../pages/members/Index';
import Settings from '../pages/settings/Index';


let routes = [
        {
            path: '/login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/release-number',
            name: 'admin.release_number',
            component: ReleaseNumber,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/release-number/add',
            name: 'admin.release_number.add',
            component: AddRelease,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/release-number/edit/:id',
            name: 'admin.release_number.edit',
            component: EditRelease,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/post',
            name: 'admin.posts',
            component: Posts,
            meta: {
                auth: true
            },
        },
        {
            path: '/admin/post/add',
            name: 'admin.posts.add',
            component: AddPost,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/post/edit/:id',
            name: 'admin.post.edit',
            component: EditPost,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/user',
            name: 'admin.user',
            component: AdminUser,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/category',
            name: 'admin.categories',
            component: Categories,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/images',
            name: 'admin.images',
            component: Images,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/member',
            name: 'admin.members',
            component: Members,
            meta: {
                auth: true
            }
        },
        {
            path: '/admin/setting',
            name: 'admin.settings',
            component: Settings,
            meta: {
                auth: true
            },
        },
        {
            path: '*',
            redirect: '/login'
        }
    ];

export default routes
