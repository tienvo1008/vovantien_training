<?php

namespace Backend\Repositories\ReleaseNumber;

use Backend\Http\Resources\ResourceReleaseNumber as ApiRelease;
use Backend\Models\ReleaseNumber;
use Backend\Repositories\EloquentRepository;
use Backend\Models\Setting;
use Exception;

class ReleaseNumberRepository extends EloquentRepository implements ReleaseNumberInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return ReleaseNumber::class;
    }

    public function getAll()
    {
        $settingPage = Setting::first();
        $per_page = empty($settingPage->release_number_page) ? 20 : $settingPage->release_number_page;

        return $this->_model->paginate($per_page);
    }

    public function arrayToJson($list)
    {
        $arr = array();
        foreach ($list as $item) {
            array_push($arr, new ApiRelease($item));
        }

        return $arr;
    }

    public function delete($id)
    {
        return $this->_model->findOrFail($id)->delete();
    }

    public function findPost($id)
    {
        return $this->_model->with('posts')->findOrFail($id);
    }

    public function find($id)
    {
        $release = $this->_model->findOrFail($id);

        return new ApiRelease($release);
    }

    public function showReleaseInPost()
    {
        return $this->_model->select('id', 'name')->get();
    }
}
