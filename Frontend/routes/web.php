<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Home'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/category/{id)', 'CategoryController@index');
    Route::get('/post-detail/{id}', 'DetailPostController@index')->name('post_detail');
});

Route::group(['namespace' => 'Ajax'], function () {
    Route::post('login', 'LoginController@postLogin');
    Route::post('logout', 'LoginController@postLogout')->name('logout');
    Route::post('register', 'LoginController@register');
});
