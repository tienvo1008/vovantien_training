<?php

namespace Frontend\Http\Controllers\Ajax;

use Auth;
use Frontend\Http\Controllers\Controller;
use Frontend\Http\Requests\LoginRequest;
use Frontend\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Frontend\Repository\Register\RegisterRepository;

class LoginController extends Controller
{
    protected $registerRepository;

    public function __construct(RegisterRepository $registerRepository)
    {
        $this->registerRepository = $registerRepository;
    }

    public function postLogin(LoginRequest $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if( Auth::guard('members')->attempt(['email' => $email, 'password' => $password])) {

            return response()->json([
                'error' => false,
                'message' => 'success'
            ], 200);
        } else {
            $errors = new MessageBag(['errorLogin' => 'ユーザー情報が正しくない。']);

            return response()->json([
                'error' => true,
                'message' => $errors
            ], 200);
        }
    }

    public function postLogout()
    {
        Auth::guard('members')->logout();

        return redirect('/');
    }

    public function register(RegisterRequest $request)
    {
        $arrUser = array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'birday' => $request->get('birday'),
            'gender' => $request->get('gender'),
        );

        $user = $this->registerRepository->create($arrUser);
        Auth::guard('members')->login($user);

        if($user) {
            return response()->json([
                'errors' => false,
            ], 200);
        } else {

            return response()->json([
                'errors' => true,
                'message' => [
                    'error' => 'Mời nhập tất cả các trường',
                 ]
            ], 400);
        }
    }
}
