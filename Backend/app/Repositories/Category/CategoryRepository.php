<?php

namespace Backend\Repositories\Category;

use Backend\Models\Category;
use Backend\Repositories\EloquentRepository;

class CategoryRepository extends EloquentRepository implements CategoryInterface
{
    /**
     * get model
     * @return string
     */
    function getModel()
    {
        return Category::class;
    }

    public function find($id)
    {
        return $this->_model->findOrFail($id);
    }

    public function delete($id)
    {
        return Category::destroy($id);
    }

    public function  getAll()
    {
        return $this->_model->with('children')->latest()->get();
    }
    public function showParent()
    {
        return $this->_model->select('id', 'name')->where('parent_id', 0)->get();
    }
    public function showChild($id)
    {
        return $this->_model->select('id', 'name')->where('parent_id', $id)->get();
    }
}
