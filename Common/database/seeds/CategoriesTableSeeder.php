<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            array( 'name' => 'MOBILE', 'global_display' => false, 'menu_display' => true, 'parent_id' => 0),
            array( 'name' => 'INTERNET', 'global_display' => false, 'menu_display' => true, 'parent_id' => 0),
            array( 'name' => 'KHÁM PHÁ', 'global_display' => false, 'menu_display' => true, 'parent_id' => 0),
            array( 'name' => 'Điện thoại', 'global_display' => true, 'menu_display' => true, 'parent_id' => 1),
            array( 'name' => 'Máy tính bảng', 'global_display' => true, 'menu_display' => true, 'parent_id' => 1),
            array( 'name' => 'Digital Maketting', 'global_display' => true, 'menu_display' => true, 'parent_id' => 2),
            array( 'name' => 'Meida', 'global_display' => true, 'menu_display' => true, 'parent_id' => 2),
            array( 'name' => 'Lịch sử', 'global_display' => true, 'menu_display' => true, 'parent_id' => 3),
            array( 'name' => 'Tri thức', 'global_display' => true, 'menu_display' => true, 'parent_id' => 3),
        ]);
    }
}
