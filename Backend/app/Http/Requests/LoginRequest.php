<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'email' => 'required|string|max:64|min:6',
            'password' => 'required|string|max:72|min:6',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'メールを入力してください。',
            'password.required' => 'パスワードを入力してください',
            'email.max' => 'メールは6文字から６４文字まで入力してください。',
            'email.min' => 'メールは6文字から６４文字まで入力してください。',
            'password.max' => 'パスワードは6文字から７２文字まで入力してください。',
            'password.min' => 'パスワードは6文字から７２文字まで入力してください。',
        ];
    }
}
