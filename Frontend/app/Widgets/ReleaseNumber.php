<?php

namespace Frontend\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Repository\ReleaseNumber\ReleaseRepository;

class ReleaseNumber extends AbstractWidget
{
    protected $releaseRepository;

    public function __construct(ReleaseRepository $releaseRepository)
    {
        $this->releaseRepository = $releaseRepository;
    }

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $release = $this->releaseRepository->getRelease();

        return view('widgets.release_number', [
            'config' => $this->config,
            'release' => $release,
        ]);
    }
}
