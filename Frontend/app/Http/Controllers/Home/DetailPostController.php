<?php

namespace Frontend\Http\Controllers\Home;

use Illuminate\Http\Request;
use Frontend\Http\Controllers\Controller;
use Frontend\Repository\Post\PostRepository;

class DetailPostController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index($id)
    {
        $post = $this->postRepository->showPostDetail($id);

        return view('post-detail.index', compact('post'));
    }

}
