<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:16|max:300',
            'public_date' => 'required|max:14',
            'content' => 'required',
            'categories_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'タイトルを入力してください。',
            'title.min' => 'タイトルは10文字から300文字まで入力してください。。',
            'title.max' => 'タイトルは10文字から300文字まで入力してください。。',
            'content.required' => '記事内容を入力してください。。',
            'categories_id.required' => '子カテゴリを入力してください。。',
        ];
    }
}
