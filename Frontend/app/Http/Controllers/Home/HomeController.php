<?php

namespace Frontend\Http\Controllers\Home;

use Frontend\Http\Controllers\Controller;
use Frontend\Repository\Post\PostRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index(Request $request)
    {
        $posts = $this->postRepository->getAll();

        if($request->ajax()) {
            $current_page = $posts->currentPage();
            $last_page = $posts->lastPage();

            $view = view('ajax.post', compact('posts'))->render();

            return response()->json([
                'html'=>$view,
                'current_page' => $current_page,
                'last_page' => $last_page
            ]);
        }

        return view('home', compact('posts'));
    }
}
