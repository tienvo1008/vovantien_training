<div class="modal" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title-login">ログイン</h4>
                <button type="button" data-dismiss="modal">閉じる</button>
            </div>
            <div class="modal-body">
                <form class="login" action="" method="POST" data-type="json">
                    @csrf
                    <p style="color:red; display:none;" class="error errorLogin"></p>
                    <div class="form-group">
                        <label>メールアドレス</label>
                        <input type="text" class="form-control" id="username" name="email" placeholder="メールの入力">
                    </div>
                    <div class="form-group">
                        <label>メールアドレス</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="パスワードの入力">
                    </div>
                    <div class="btn-login">
                        <button type="submit" class="login" id="login">ログイン</button>
                        <button type="button" class="close-login" data-dismiss="modal">キャンセル</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
