<?php

namespace Backend\Repositories\Images;

interface ImageInterface
{
    public function delete($id);

    public function create(array $attributes);

    public function find($id);
}
