<div class="carousel-inner">
    @foreach($release as $key=>$value)
        <div class="carousel-item {{$key==0 ? 'active' : ''}}">
            <div class="panner">
                <div class="container-home">
                    <div class="block">
                        <div class="block-left">
                            <div class="content">
                                <span>発売号</span>
                                <h2>
                                    {!! $value->name !!}
                                </h2>
                                <p>
                                    {!! $value->description !!}
                                </p>
                                <button class="button" href="#carouselExampleControls" role="button" data-slide="next">
                                    次
                                </button>
                            </div>
                        </div>
                        <div class="block-right">
                            <img src="{!! 'http://adminsite.local' . $value->image->url !!}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
