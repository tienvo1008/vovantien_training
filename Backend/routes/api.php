<?php

Route::post('auth/register', 'Auth\AuthController@register');
Route::post('auth/login', 'Auth\AuthController@login');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('auth/user', 'Auth\AuthController@user');
    Route::post('auth/logout', 'Auth\AuthController@logout');
});

Route::group(['middleware' => 'jwt.refresh'], function () {
    Route::get('auth/refresh', 'Auth\AuthController@refresh');
});

Route::group(['namespace' => 'ReleaseNumber'], function () {
    Route::resource('release-number', 'ReleaseNumberController');
    Route::get('release-post', 'ReleaseNumberController@showReleaseInPost');
});

Route::group(['namespace' => 'Image'], function () {
    Route::resource('image', 'ImageController');
});

Route::group(['namespace' => 'Post'], function () {
    Route::resource('post', 'PostController');
});

Route::group(['namespace' => 'Category'], function () {
    Route::resource('category', 'CategoryController');
    Route::get('category-parent', 'CategoryController@showParent');
    Route::get('category-child/{id}', 'CategoryController@showChild');
});

