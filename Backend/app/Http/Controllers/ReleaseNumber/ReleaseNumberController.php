<?php

namespace Backend\Http\Controllers\ReleaseNumber;

use Backend\Http\Controllers\Controller;
use Backend\Http\Requests\AddReleaseRequest;
use Backend\Http\Requests\UpdateReleaseRequest;
use Backend\Repositories\Images\ImageRepository;
use Backend\Repositories\Post\PostRepository;
use Backend\Repositories\ReleaseNumber\ReleaseNumberInterface;
use Illuminate\Support\Facades\Storage;
use Exception;

class ReleaseNumberController extends Controller
{
    /**
     * @var ReleaseNumberInterface|\Backend\Repositories\Repository
     */

    protected $releaseNumberRepository;

    protected $imageRepository;

    protected $postRepository;

    public function __construct(ReleaseNumberInterface $releaseNumberRepository,
                                ImageRepository $imageRepository, PostRepository $postRepository)
    {
        $this->releaseNumberRepository = $releaseNumberRepository;
        $this->imageRepository = $imageRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $release = $this->releaseNumberRepository->getAll();
            $response = [
                'pagination' => [
                    'total' => $release->total(),
                    'per_page' => $release->perPage(),
                    'current_page' => $release->currentPage(),
                    'last_page' => $release->lastPage(),
                    'from' => $release->firstItem(),
                    'to' => $release->lastItem()
                ],
                'data' => $this->releaseNumberRepository->arrayToJson($release),
            ];
            return response()->json($response);

        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。',
                ]
            ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddReleaseRequest $request)
    {
        try{
            $arrRelease = array(
                'name' => $request->get('name'),
                'image_id' => $request->get('image_id'),
                'description' => $request->get('description'),
                'thumbnail' => 'null',
            );

            $release = $this->releaseNumberRepository->create($arrRelease);

            if($release) {
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => false,
                    ]
                ]);
            }
            else {
                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => '情報処理の中に、エーラが発生しました',
                    ]
                ]);
            }
        } catch (Exception $exception) {
            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->releaseNumberRepository->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReleaseRequest $request, $id)
    {
        try {
            $release = $this->releaseNumberRepository->find($id);
            $old_image_id = $release->image_id;
            $image = $this->imageRepository->find($old_image_id);
            $url = str_replace('/storage/', '/public/', $image->url);

            $arrRelease = array(
                'name' => $request->get('name'),
                'image_id' => $request->get('image_id'),
                'description' => $request->get('description'),
                'thumbnail' => 'null',
            );

            $release = $this->releaseNumberRepository->update($id, $arrRelease);

            if ($release) {
                if($old_image_id !== $arrRelease['image_id']) {
                    $this->imageRepository->delete($image->id);
                    Storage::delete($url);
                }

                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => false
                    ]
                ]);
            } else {

                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => '情報処理の中に、エーラが発生しました。',
                    ]
                ]);
            }
        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました',
                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $release = $this->releaseNumberRepository->find($id);
            $image_id = $release->image_id;
            $image = $this->imageRepository->find($image_id);
            $url = str_replace('/storage/', '/public/', $image->url);
            $this > $this->imageRepository->delete($image->id);
            Storage::delete($url);
            if ($this->releaseNumberRepository->delete($id)) {

                return response()->json([
                    'data' => '',
                    'error' => [
                        'status' => false,
                    ]
                ]);
            }
        } catch (Exception $exception) {

            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'message' => '情報処理の中に、エーラが発生しました。',
                ]
            ]);
        }
    }
    public function showReleaseInPost()
    {
        try {
            $post = $this->releaseNumberRepository->showReleaseInPost();

            return response()->json([
                'data' => $post,
                'error' => [
                    'status' => false,
                ]
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'data' => '',
                'error' => [
                    'status' => true,
                    'message' => '情報処理の中に、エーラが発生しました。',
                ]
            ]);
        }
    }
}
