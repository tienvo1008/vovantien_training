<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddReleaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:16',
            'image_id' => 'required',
            'description' => 'max:500',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '発売号を入力してください',
            'name.max' => '発売号が16文字以下み有効です。',
            'image_id.required' => '発売号画像を入力してください。',
            'description.max' => '形容が500文字以下み有効です。',
        ];
    }
}
