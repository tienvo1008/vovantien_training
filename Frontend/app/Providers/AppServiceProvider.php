<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Frontend\Models\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layout.header', function($view){
            $cateMenuDisplay = Category::select('id', 'name')->with('children')->where('menu_display', 1)->get();
            $cateMenuGlobal = Category::select('id', 'name')->with('children')->where('global_display', 0)->get();
            $view->with(['cateMenuDisplay'=>$cateMenuDisplay,'cateMenuGlobal'=>$cateMenuGlobal]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
